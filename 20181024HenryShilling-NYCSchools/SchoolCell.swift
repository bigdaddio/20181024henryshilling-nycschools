//
//  SchoolCell.swift
//  20181024HenryShilling-NYCSchools
//
//  Created by Henry Shilling on 10/25/18.
//  Copyright © 2018 Henry Shilling. All rights reserved.
//

import UIKit

class SchoolCell: UITableViewCell {

    @IBOutlet weak var SchoolName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
