//
//  SchoolDetailVC.swift
//  20181024HenryShilling-NYCSchools
//
//  Created by Henry Shilling on 10/25/18.
//  Copyright © 2018 Henry Shilling. All rights reserved.
//

import UIKit

class SchoolDetailVC: UIViewController {

    var SchoolSatData: SATData?
    
    @IBOutlet weak var SchoolName: UILabel!
    @IBOutlet weak var StudentCount: UILabel!
    @IBOutlet weak var ReadingScore: UILabel!
    @IBOutlet weak var MathScore: UILabel!
    @IBOutlet weak var WritingScore: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        SchoolName.text = SchoolSatData?.school_name
        StudentCount.text = SchoolSatData?.num_of_sat_test_takers
        ReadingScore.text = SchoolSatData?.sat_critical_reading_avg_score
        MathScore.text = SchoolSatData?.sat_math_avg_score
        WritingScore.text = SchoolSatData?.sat_writing_avg_score
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
