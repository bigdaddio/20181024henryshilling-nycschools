//
//  SchoolsList.swift
//  20181024HenryShilling-NYCSchools
//
//  Created by Henry Shilling on 10/25/18.
//  Copyright © 2018 Henry Shilling. All rights reserved.
//

// I would believe that for a real application I would set up a cache system at the least
// using core data. At best I have used systems like Azure mobile services for data sync
// This is a pretty simple system, the SATData codeable is probably overkill.


import UIKit

struct SATData: Codable {
    let school_name: String?
    let num_of_sat_test_takers: String?
    let sat_critical_reading_avg_score: String?
    let sat_math_avg_score: String?
    let sat_writing_avg_score: String?
    
    private enum CodingKeys: String, CodingKey {
        case school_name
        case num_of_sat_test_takers
        case sat_critical_reading_avg_score
        case sat_math_avg_score
        case sat_writing_avg_score
    }
}

class SchoolsList: UITableViewController {

    var satData: [SATData] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return satData.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellReuseIdentifier", for: indexPath) as! SchoolCell

        let text = satData[indexPath.row].school_name!
        cell.SchoolName.text = text

        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "SchoolDetailSegue", sender: self)
    }
    
    // MARK: data
    func getData() {
        guard let gitUrl = URL(string: "https://data.cityofnewyork.us/resource/734v-jeq5.json") else { return }
        
        URLSession.shared.dataTask(with: gitUrl) { (data, response
            , error) in
            guard let data = data else { return }
            do {
                let decoder = JSONDecoder()
                self.satData = try decoder.decode([SATData].self, from: data)
                
                DispatchQueue.main.sync {
                    self.satData = self.satData.sorted { $0.school_name?.localizedCaseInsensitiveCompare($1.school_name!) == ComparisonResult.orderedAscending }

                    self.tableView.reloadData()
                }
            } catch let err {
                print("Err", err)
            }
            }.resume()
    }

    
    // MARK: - Navigation

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let destination = segue.destination as! SchoolDetailVC
        let selectedRow = tableView.indexPathForSelectedRow?.row
        destination.SchoolSatData = satData[selectedRow!]
    }
 

}
